import configparser
import datajoint as dj
from dj_tables import *

Config = configparser.ConfigParser()
configFilePath = "config.ini"
Config.read(configFilePath)
SectionName = "DataJoint"
dj.config["database.host"] = Config.get(SectionName, "database.host")
dj.config["database.user"] = Config.get(SectionName, "database.user")
dj.config["database.password"] = Config.get(SectionName, "database.password")
dj.config.save_local()
# dj.set_password()
print(dj.conn())


# username = dj.conn().conn_info["user"]
# schema = dj.schema(f"{username.replace('u_', '')}_koulakov", locals())
# dj.ERD(schema).draw()
